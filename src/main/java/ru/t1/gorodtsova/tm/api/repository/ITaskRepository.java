package ru.t1.gorodtsova.tm.api.repository;

import ru.t1.gorodtsova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    void clear();

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task project);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

}
