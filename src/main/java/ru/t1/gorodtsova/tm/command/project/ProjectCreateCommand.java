package ru.t1.gorodtsova.tm.command.project;

import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Create new project";

    private final String NAME = "project-create";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().create(name, description);
    }

}
